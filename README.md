Need to parse a json API and display the products in a grid. The grid should have the following filtering options

      ### By Product Item Code
      ### By Product E-Code
      ### By Part Name
      
Languages used: create react app

URL: https://spreadsheets.google.com/feeds/list/1NPC0myYBmPuxaQuxPyo5WyxYEMpQU2lnI3SpapI1AwU/1/public/values?alt=json

| Product Item Code | Product E-Code | Product Description         | PSG | Part Number | Part Name | Spare Item code | Spare Description                      | Qty | DISABLE_DATE |
|-------------------|----------------|-----------------------------|-----|-------------|-----------|-----------------|----------------------------------------|-----|--------------|
| ACHMTIA75260G8    | 19165          | 10HP CTG-43/10D JUMBO 65x50 | A1A | 32.25       | Sticker   | RS6BCLX441      | STICKER : BCL PLAIN 75*12mm POLYESTRER | 0.5 | 18-Feb-17    |
| ACHMTIA75260G8    | 19165          | 10HP CTG-43/10D JUMBO 65x50 | A1A | 32.25       | Sticker   | 283845          | STICKER : BCL PLAIN 75*12mm POLYESTRER | 0.5 |              |


      

